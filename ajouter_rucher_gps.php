<?php
/* Handle CORS */

// Specify domains from which requests are allowed
header('Access-Control-Allow-Origin: localhost:19006');

// Specify which request methods are allowed
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

// Additional headers which may be sent along with the CORS request
header('Access-Control-Allow-Headers: X-Requested-With,Authorization,Content-Type');

// Set the age to 1 day to improve speed/caching.
header('Access-Control-Max-Age: 86400');

// Exit early so the page isn't fully loaded for options requests
if (strtolower($_SERVER['REQUEST_METHOD']) == 'options') {
    exit();
}

// Importing DBConfig.php file.
include 'dbconfig.php';

// Creating connection.
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);

// Getting the received JSON into $json variable.
$json = file_get_contents('php://input');

// decoding the received JSON and store into $obj variable.
$obj = json_decode($json,true);

 // Populate User name from JSON $obj array and store into $name.
 $latitude= $obj['latitude'];
 
 // Populate User email from JSON $obj array and store into $email.
 $longitude=$obj['longitude'];;
  
 // Populate Password from JSON $obj array and store into $password.
 $name = $obj['name'];
 $numerorue = null;
 $nomrue = null;
 $CP = null;
 $ville = null;
 $idRucher= 5;

 //Checking Email is already exist or not using SQL query.
$CheckSQL = "SELECT * FROM rucher WHERE name ='$name'";
 

// Executing SQL Query.
$check = mysqli_fetch_array(mysqli_query($con,$CheckSQL));


 if(isset($check)){
 
    $nameExistMSG = 'ce nom existe deja, essayez un nouveau !!!';
    
    // Converting the message into JSON format.
   $nameExistJson = json_encode($nameExistMSG);
    
   // Echo the message.
    echo $nameExistJson; 

    }

    else{
 
        if( (empty($name)) || (empty($latitude)) || (empty($longitude)) ){
            $vide = 'tous les champs ne sont pas remplis !!!';
            $videjson = json_encode($vide);
            echo $videjson;
        }
        
        else{
        // Creating SQL query and insert the record into MySQL database table.
       $Sql_Query = "insert into rucher (latitude,longitude, idUser, name,nomrue,CP,numerorue,ville) 
       values ('$latitude','$longitude','1','$name','$nomrue','$CP','$numerorue','$ville')";
        
       //echo ($latitude, $longitude, $name);
        if(mysqli_query($con,$Sql_Query)){
        //$CheckSQL = "SELECT * FROM rucher";
        //$check2 = mysqli_fetch_array(mysqli_query($con,$CheckSQL));
        //echo $check2['numerorue'];
        // If the record inserted successfully then show the message.
       $MSG = 'nouveau rucher enregistre' ;
        
       // Converting the message into JSON format.
       $json = json_encode($MSG);
        
       // Echo the message.
        echo $json ;
        
        }
       
        else{
        
        echo 'Try Again';
        
        }
    }
}
        mysqli_close($con);
 
?>
