<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");
// Importing DBConfig.php file.
include 'dbconfig.php';

// Creating connection.
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
$bdd = new PDO('mysql:host=localhost;dbname=beesmart;charset=utf8', 'root', '');
    
$json = file_get_contents('php://input');

// decoding the received JSON and store into $obj variable.
$obj = json_decode($json,true);

 // Populate User name from JSON $obj array and store into $name.
 $nombre_cadre= $obj['nombre_cadre'];
 
 // Populate User email from JSON $obj array and store into $email.
 $taille=$obj['taille'];
  
 // Populate Password from JSON $obj array and store into $password.
 $numero_serie= $obj['numero_serie'];
 $idRucher= $obj['idrucher'];

 //Checking Email is already exist or not using SQL query.

$CheckSQL = "SELECT  numero_serie FROM ruche WHERE numero_serie ='$numero_serie'";


// Executing SQL Query.
$check = mysqli_fetch_array(mysqli_query($con,$CheckSQL));


 if(isset($check)){
 
    $numeroExistMSG = 'ce numero existe deja, essayez un nouveau !!!';
    
    // Converting the message into JSON format.
   $numeroExistJson = json_encode($numeroExistMSG);
    
   // Echo the message.
    echo $numeroExistJson; 

    }

    else{
 
        if(empty($numero_serie)||(empty($idRucher))){
            $vide = 'vous devez renseigner au moins le rucher et le numero de serie!!!';
            $videjson = json_encode($vide);
            echo $videjson;
        }
        
                else{
                                // Creating SQL query and insert the record into MySQL database table.
                            $Sql_Query = "insert into ruche (nombre_cadre,taille,numero_serie,idRucher) 
                            values ('$nombre_cadre','$taille','$numero_serie','$idRucher')";
                                
                            //echo ($latitude, $longitude, $name);
                            if(mysqli_query($con,$Sql_Query)){
                                //$CheckSQL = "SELECT * FROM rucher";
                                //$check2 = mysqli_fetch_array(mysqli_query($con,$CheckSQL));
                                //echo $check2['numerorue'];
                                // If the record inserted successfully then show the message.
                            $MSG = 'nouvelle ruche enregistrée' ;
     
                                
                            // Converting the message into JSON format.
                            $json = json_encode($MSG);
                                
                            // Echo the message.
                                echo $json ;
        
                                }
       
                            else{
                            
                            echo 'Try Again';
                            
                            }
                }
    }
    
        mysqli_close($con);
 
?>